package nrfiles

import (
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
)

// CloseFile will close the provided file swallowing any error produced.
// This function is intended to be called with defer and is primarily intended
// to remove IDE warnings caused by calling defer file.Close()
func CloseFile(file *os.File) {
	_ = file.Close()
}

// DirExists checks if the given path exits and is a directory.
func DirExists(path ...string) bool {
	dir := filepath.Join(path...)
	fileInfo, err := os.Stat(dir)
	if err != nil {
		return false
	}
	return fileInfo.IsDir()
}

// FileExists checks if the given path exists and is not a directory.
func FileExists(path ...string) bool {
	file := filepath.Join(path...)
	fileInfo, err := os.Stat(file)
	if err != nil {
		return false
	}
	return !fileInfo.IsDir()
}

// DirEmpty checks if the give directory path is empty of files and other
// directories. This function will return an error if the directory does
// not exists or is a file
func DirEmpty(path ...string) (bool, error) {
	dir := filepath.Join(path...)
	file, err := os.Open(dir)
	if err != nil {
		return false, err
	}
	defer CloseFile(file)
	_, err = file.Readdirnames(1)
	if err == io.EOF {
		return true, nil
	}
	return false, nil
}

// CopyFile copies the contents of the source file (src) to the destination
// file (dst). The file will be created if it doesn't exit and will be
// overwritten if it already exists. The number of bytes wirten is returned.
func CopyFile(src, dst string) (int64, error) {
	in, err := os.Open(src)
	if err != nil {
		return 0, err
	}
	defer CloseFile(in)

	out, err := os.Create(dst)
	if err != nil {
		return 0, err
	}
	defer CloseFile(out)

	w, err := io.Copy(out, in)
	if err != nil {
		return 0, nil
	}

	err = out.Sync()
	if err != nil {
		return 0, err
	}

	si, err := os.Stat(src)
	if err != nil {
		return w, err
	}

	err = os.Chmod(dst, si.Mode())
	if err != nil {
		return w, err
	}

	return w, nil
}

// CopyDir copies a directory preserving permissions. This function will
// return an error if it encounters a symlink.
func CopyDir(src string, dst string) (err error) {
	src = filepath.Clean(src)
	dst = filepath.Clean(dst)

	if !DirExists(src) {
		return fmt.Errorf("source is not a directory or does not exist")
	}

	if FileExists(dst) {
		return fmt.Errorf("destination is a file")
	}

	if DirExists(dst) {
		return fmt.Errorf("destination already exits")
	}

	ss, _ := os.Stat(src)
	err = os.MkdirAll(dst, ss.Mode())
	if err != nil {
		return
	}

	entries, err := ioutil.ReadDir(src)
	if err != nil {
		return
	}

	for _, entry := range entries {
		srcPath := filepath.Join(src, entry.Name())
		dstPath := filepath.Join(dst, entry.Name())

		if entry.IsDir() {
			err = CopyDir(srcPath, dstPath)
			if err != nil {
				return
			}
		} else {
			if entry.Mode()&os.ModeSymlink != 0 {
				return fmt.Errorf("unable to copy symlink %s", srcPath)
			}
			_, err = CopyFile(srcPath, dstPath)
			if err != nil {
				return
			}
		}
	}

	return
}
