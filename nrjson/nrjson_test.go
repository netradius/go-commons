package nrjson

import (
	"fmt"
	"math/rand"
	"os"
	"testing"
)

type Example1 struct {
	Field1 string `json:"field1"`
	Field2 int    `json:"field2"`
}
type Example2 struct {
	FieldA   string   `json:"fieldA"`
	Example1 Example1 `json:"example1"`
}

func getExample() Example2 {
	return Example2{
		FieldA: "Field A on Example 2",
		Example1: Example1{
			Field1: "Field 1 on Example 1 ",
			Field2: rand.Int(),
		},
	}
}

func TestPrettyPrint(t *testing.T) {
	fmt.Println("Testing PrettyPrint")
	PrettyPrint(getExample())
}

func TestWriteFile(t *testing.T) {
	fmt.Println("Testing WriteFile")
	file := "/tmp/nrjson-write.tmp"
	ex := getExample()
	err := WriteFile(ex, file)
	if err != nil {
		t.Fatal(err)
	}
	defer func() {
		_ = os.Remove(file)
	}()
}

func TestReadFile(t *testing.T) {
	fmt.Println("Testing ReadFile")
	file := "/tmp/nrjson-read.tmp"
	ex := getExample()
	err := WriteFile(ex, file)
	if err != nil {
		t.Fatal(err)
	}
	defer func() {
		_ = os.Remove(file)
	}()
	ex = Example2{}
	err = ReadFile(ex, file)
	if err != nil {
		t.Fatal(err)
	}
}
