package nrjson

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
)

func PrettyPrint(v interface{}) {
	json, err := json.MarshalIndent(v, "", "  ")
	if err != nil {
		log.Printf("error pretty printing json: %v\n", err)
	}
	fmt.Println(string(json))
}

func ReadFile(v interface{}, file string) (err error) {
	f, err := os.Open(file)
	if err != nil {
		return
	}
	defer func() {
		err = f.Close()
	}()
	b, err := ioutil.ReadAll(f)
	if err != nil {
		return
	}
	return json.Unmarshal(b, v)
}

func WriteFile(v interface{}, file string) (err error) {
	return WriteFilePerm(v, file, 0644)
}

func WriteFilePerm(v interface{}, file string, perm os.FileMode) (err error) {
	b, err := json.Marshal(v)
	if err != nil {
		return
	}
	return ioutil.WriteFile(file, b, perm)
}
