###############################################################################
# Go Helpers Makefile
###############################################################################
.PHONY: build clean format test

# Remember make files use tabs to identify commands therefore these top
# sections use spaces for indentation.

#------------------------------------------------------------------------------
# Operating System & Architecture Detection
#------------------------------------------------------------------------------
# This section is responsible for detecting operating system and architecture.
#
# Be aware we do not use uname to detect the architecture. We instead check
# the ls binary to see if it is 64-bit or 32-bit. This works much better in
# builds using things like docker images which may run on a 64bit OS, but can
# be 32bit images.

DETECTED_OS := unknown
DETECTED_ARCH := unknown
ifeq "Windows_NT" "$(OS)"
  DETECTED_OS := windows
  ifeq ($(PROCESSOR_ARCHITECTURE),AMD64)
  	DETECTED_ARCH := amd64
  endif
  ifeq ($(PROCESSOR_ARCHITECTURE),x86)
  	DETECTED_ARCH := i386
  endif
else
  UNAME_S := $(shell uname -s)
  ifeq ($(UNAME_S),Linux)
    DETECTED_OS := linux
  endif
  ifeq ($(UNAME_S),Darwin)
    DETECTED_OS := darwin
  endif
  FILE_BIT := $(shell file $(shell command -v file 2> /dev/null) | awk '{print $$3}')
  ifeq ($(FILE_BIT),64-bit)
    DETECTED_ARCH := amd64
  endif
  ifeq ($(FILE_BIT),32-bit)
  	DETECTED_ARCH := i386
  endif
endif
$(info Detected Operating System:  $(DETECTED_OS))
$(info Detected Architecture:      $(DETECTED_ARCH))
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# Install Prefix
#------------------------------------------------------------------------------
ifeq ($(PREFIX),)
    PREFIX := ./install
endif
#------------------------------------------------------------------------------

build:
	env CGO_ENABLED=0 GOOS=$(DETECTED_OS) GOARCH=$(DETECTED_ARCH) go build ./...

clean:
	rm -rf bin

format:
	gofmt -w .

test:
	env CGO_ENABLED=0 GOOS=$(DETECTED_OS) GOARCH=$(DETECTED_ARCH) go test ./...
