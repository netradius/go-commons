# Go Commons

This project consists of a number of Go modules to provide common functions
often repeated, copied and reproduced across Go applications.

To get one of the modules in this repository run the following

```bash
go get -u bitbucket.org/netradius/go-commons/<module>
```

Replace <module> with the name of the module you want to retrieve.
