package nroidc

import (
	"fmt"
	"testing"
)

func TestAuthorizer(t *testing.T) {

	tres, err := NewClientCredentialsToken(
		// TODO Update when we have an authority to use for unit tests
		"https://auth.example.com",
		"example-id",
		"example-secret",
		"example-scope")
	if err != nil {
		t.Errorf("error getting token: %s\n", err)
	}

	a, err := NewAuthorizer("https://auth.example.com", "example-audience")
	if err != nil {
		t.Errorf("error creating authorizer: %s\n", err)
	}

	_, err = a.Authorize(tres.AccessToken)
	if err != nil {
		t.Errorf("authorization failed: %s\n", err)
	} else {
		fmt.Print("token successfully verified!")
	}
}
