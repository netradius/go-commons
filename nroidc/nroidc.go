package nroidc

import (
	"bytes"
	"context"
	"encoding/json"
	"github.com/lestrrat-go/jwx/jwk"
	"github.com/lestrrat-go/jwx/jwt"
	"io/ioutil"
	"net/http"
	"net/url"
	"time"
)

const (
	ConfigurationPath          = "/.well-known/openid-configuration"
	DefaultAcceptableClockSkew = 5 * time.Minute
	MinimumRefreshInterval     = 15 * time.Minute
)

type WellKnownConfig struct {
	Issuer        string `json:"issuer"`
	JwksUri       string `json:"jwks_uri"`
	TokenEndpoint string `json:"token_endpoint"`
}

func NewWellKnownConfig(authority string) (*WellKnownConfig, error) {
	httpcl := http.DefaultClient
	res, err := httpcl.Get(authority + ConfigurationPath)
	if err != nil {
		return nil, err
	}
	b, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}
	var config WellKnownConfig
	err = json.Unmarshal(b, &config)
	if err != nil {
		return nil, err
	}
	return &config, nil
}

type Authorizer struct {
	WellKnownConfig *WellKnownConfig
	AutoRefresh     *jwk.AutoRefresh
	Audience        string
	Authority       string
}

func NewAuthorizer(authority string, audience string) (*Authorizer, error) {
	c, err := NewWellKnownConfig(authority)
	if err != nil {
		return nil, err
	}

	ctx := context.Background()
	ar := jwk.NewAutoRefresh(ctx)
	ar.Configure(c.JwksUri, jwk.WithMinRefreshInterval(MinimumRefreshInterval))
	_, err = ar.Refresh(ctx, c.JwksUri)
	if err != nil {
		return nil, err
	}

	a := Authorizer{
		c,
		ar,
		audience,
		authority,
	}

	return &a, nil
}

func (a Authorizer) Authorize(token string) (*jwt.Token, error) {
	keyset, err := a.AutoRefresh.Fetch(context.Background(), a.WellKnownConfig.JwksUri)
	if err != nil {
		return nil, err
	}

	t, err := jwt.Parse(bytes.NewReader([]byte(token)),
		jwt.WithKeySet(keyset),
		jwt.WithValidate(true),
		jwt.WithAudience(a.Audience),
		jwt.WithAcceptableSkew(DefaultAcceptableClockSkew),
	)
	return &t, err
}

type TokenEndpointResponse struct {
	AccessToken  string `json:"access_token"`
	TokenType    string `json:"token_type"`
	RefreshToken string `json:"refresh_token"`
	ExpiresIn    int    `json:"expires_in"`
	IdToken      string `json:"id_token"`
}

func NewClientCredentialsToken(authority string, id string, secret string, scopes ...string) (*TokenEndpointResponse, error) {
	c, err := NewWellKnownConfig(authority)
	if err != nil {
		return nil, err
	}

	form := url.Values{
		"grant_type":    {"client_credentials"},
		"client_id":     {id},
		"client_secret": {secret},
		"scope":         scopes,
	}
	httpcl := http.DefaultClient
	res, err := httpcl.PostForm(c.TokenEndpoint, form)
	if err != nil {
		return nil, err
	}

	b, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	var tres TokenEndpointResponse
	err = json.Unmarshal(b, &tres)
	return &tres, err
}
